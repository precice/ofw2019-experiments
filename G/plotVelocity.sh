#! /bin/bash
gnuplot -p << EOF
set grid
set xlabel 'Time [s]'
set ylabel 'Velocity'
set key right bottom
#set xrange [0.0:0.5]
plot "Fluid/postProcessing/probes/0/U" using 1:6 with lines title "1D-3D-1D", \
     "../B/results/II/U" using 1:6 with lines title "monolithic 3D"
EOF

