#! /bin/bash
gnuplot -p << EOF
set grid
set xlabel 'Time [s]'
set ylabel 'Pressure'
set xrange [6.8:8.2]
set yrange [280000:310000]

plot "results/I/p" using 1:2 with lines title "CN 0.45", \
     "results/III/p" using 1:2 with lines title "small dt", \
     "results/II/p" using 1:2 with lines title "CN 0.5"
EOF

