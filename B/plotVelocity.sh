#! /bin/bash
gnuplot -p << EOF
set grid
set xlabel 'Time [s]'
set ylabel 'Velocity'
plot "results/I/U" using 1:6 with lines title "CN 0.45", \
     "results/II/U" using 1:6 with lines title "CN 0.5"
EOF

