#! /bin/bash
gnuplot -p << EOF
set grid
set xlabel 'Time [s]'
set ylabel 'Pressure'
set xrange [0.0:20]
#set yrange [280000:310000]

plot "results/I/p2" using 1:2 with lines title "only values", \
     "results/II/p2" using 1:2 with lines title "both gradients", \
     "results/p_mono" using 1:2 with lines title "monolithic", \
      "results/III/p2" using 1:2 with lines title "one gradient"

EOF

