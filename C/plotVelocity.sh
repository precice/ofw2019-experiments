#! /bin/bash
gnuplot -p << EOF
set grid
set xlabel 'Time [s]'
set ylabel 'Velocity'
set key right bottom
plot "Fluid1/postProcessing/probes/0/U" using 1:3 with lines title "OpenFOAM-left", \
     "Fluid2/postProcessing/probes/0/U" using 1:6 with lines title "OpenFOAM-right", \
     "results/U_mono" using 1:6 with lines title "monolithic"
#plot "results/watchpointLeft1.txt" using 1:5 with lines title "coupled_left", \
#     "results/watchpointRight1.txt" using 1:3 with lines title "coupled_right", \
#     "results/watchpoint3.txt" using 1:3 with lines title "monolithic"
EOF

