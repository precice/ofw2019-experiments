#! /bin/bash
gnuplot -p << EOF
set grid
set xlabel 'Time [s]' font ",12"
set ylabel 'Velocity [m/s]' font ",12" offset -0
set key center bottom
set encoding utf8
set xtics font ",12" 
set ytics font ",12" 
set key font ",14"
#set format y "%.1t*10^%T";
set lmargin 15

set xrange [0:20]
#set xrange [13.5:15.5]
#set yrange [0.99:1.21]
set yrange [0:1.3]


plot "2/U_right" using 1:6 with lines dt 1 lc rgb "grey" lw 2 title "3D Explicit Coupling", \
     "3/U_right" using 1:6 with lines dt 2 lc rgb "dark-grey" lw 2 title "3D Implicit Coupling", \
     "../../B/results/II/U" using 1:6 with lines dt 4 lc rgb "gray40" lw 2 title "3D Monolithic"


EOF

