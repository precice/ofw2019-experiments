#! /bin/bash
gnuplot -p << EOF
set grid
set xlabel 'Time [s]' font ",12"
set ylabel 'Pressure [Pa]' font ",12" offset -3
#set key center bottom 
set key at graph 0.9, 0.2
set encoding utf8
set xtics font ",12" 
set ytics font ",12" 
set key font ",14" 
set format y "%.1t*10^%T";
set lmargin 15

#set xrange [6.8:8.2]
set xrange [0:20]
set yrange [-350000:350000]
#set yrange [200000:330000]

#plot "2/p_right" using 1:2 with lines dt 1 lc rgb "grey" lw 2 title "3D Explicit Coupling", \
#     "3/p_right" using 1:2 with lines dt 2 lc rgb "dark-grey" lw 2 title "3D Implicit Coupling", \
#     "../../B/results/II/p" using 1:2 with lines dt 4 lc rgb "gray40" lw 2 title "3D Monolithic"
     


plot "II/p2" using 1:2 with lines dt 1 lc rgb "grey" lw 2title "3D Both Grad.", \
     "III/p2" using 1:2 with lines dt 2 lc rgb "dark-grey" lw 2 title "3D Only Pressure Grad.", \
     "../../B/results/II/p" using 1:2 with lines dt 4 lc rgb "gray40" lw 2 title "3D Monolithic"


EOF

