#! /usr/bin/python3

from nutils import *
import numpy, itertools, treelog
import precice_future as precice
#import precice
from mpi4py import MPI

@function.replace
def subs0(f):
  if isinstance(f, function.Argument) and f._name == 'lhs':
    return function.Argument(name='lhs0', shape=f.shape, nderiv=f._nderiv)

def main(nelems=100, dt=.005, refdensity=1e3, refpressure=101325., psi=1e-6, viscosity=1e-3, theta=.55):

  #domain, geom = mesh.rectilinear([numpy.linspace(0, 1000, nelems+1), numpy.linspace(0, 1, 6)])
  domain, geom = mesh.rectilinear([numpy.linspace(250, 750, nelems+1)])
    
  bezier = domain.sample('bezier', 2)  
  

  ns = function.Namespace()
  ns._functions['t'] = lambda f: theta * f + (1-theta) * subs0(f)
  ns._functions_nargs['t'] = 1
  ns._functions['δt'] = lambda f: (f - subs0(f)) / dt
  ns._functions_nargs['δt'] = 1
  ns.x = geom
  ns.ρref = refdensity
  ns.pref = refpressure
  ns.pin = 98100
  ns.μ = viscosity
  ns.ψ = psi
  ns.ρbasis, ns.ubasis = function.chain([domain.basis('std', degree=1), domain.basis('std', degree=2).vector(domain.ndims)])
  ns.ρ = 'ρref + ρbasis_n ?lhs_n' # density is shifted by ref density
  ns.u_i = 'ubasis_ni ?lhs_n'
  ns.p = 'pref + (ρ - ρref) / ψ' # pressure-density connection, see equ (8)
  ns.σ_ij = 'μ (u_i,j + u_j,i) - p δ_ij' # diffusive term and pressure gradient
  ns.h = 1 / nelems
  ns.k = 'ρ h / μ' # needs work, stabilization coeff

  res = domain.integral('ρbasis_n (δt(ρ) + t((ρ u_k)_,k)) d:x' @ ns, degree=4) # mass balance
  res += domain.integral('(ubasis_ni (δt(ρ u_i) + t((ρ u_i u_j)_,j)) + ubasis_ni,j t(σ_ij)) d:x' @ ns, degree=4) # momentum balance
  
  configFileName = "precice-config.xml"
  participantName = "Nutils"
  meshNameLeft = participantName + "-Mesh-Left"
  meshNameRight = participantName + "-Mesh-Right"
  solverProcessIndex = 0
  solverProcessSize = 1

  interface = precice.Interface(participantName, solverProcessIndex, solverProcessSize)
  interface.configure(configFileName)
      
  meshLeftID = interface.get_mesh_id(meshNameLeft)
  meshRightID = interface.get_mesh_id(meshNameRight)

  writeDataLeft = "VelocityLeft" 
  readDataLeft = "PressureLeft" 
  writedataLeftID = interface.get_data_id(writeDataLeft, meshLeftID)
  readdataLeftID = interface.get_data_id(readDataLeft, meshLeftID)
  
  writeDataRight = "PressureRight" 
  readDataRight = "VelocityRight" 
  writedataRightID = interface.get_data_id(writeDataRight, meshRightID)
  readdataRightID = interface.get_data_id(readDataRight, meshRightID)

  couplinginterfaceLeft = domain.boundary['left']
  couplinginterfaceRight = domain.boundary['right']

  vertexLeft = numpy.array([0.0, 250.0, 0.0])
  dataIndexLeft = interface.set_mesh_vertex(meshLeftID, vertexLeft)
  
  vertexRight = numpy.array([0.0, 750.0, 0.0])
  dataIndexRight = interface.set_mesh_vertex(meshRightID, vertexRight)

  precice_dt = interface.initialize()  
  
  

  lhs = numpy.zeros(res.shape)
  t = 0
  n = 0
  
  f = open("watchpoint.txt", "w")
  
  res0 = res
  lhs0 = numpy.zeros(res.shape)
  
  while interface.is_coupling_ongoing():
    with log.context('dt', n):
    
      readdataRight = interface.read_vector_data(readdataRightID, dataIndexRight)
      uOut2 = readdataRight[1]
      
      readdataLeft = interface.read_scalar_data(readdataLeftID, dataIndexLeft)
      ns.pin = readdataLeft
        
      sqr = domain.boundary['right'].integral('(u_0 - ?uOut)^2' @ ns, degree=4)
      cons = solver.optimize('lhs', sqr, arguments=dict(uOut=uOut2), droptol=1e-14)  
      
      res = res0 + domain.boundary['left'].integral('pin ubasis_ni n_i d:x' @ ns, degree=4)
        
        
      # save checkpoint
      if interface.is_action_required(precice.action_write_iteration_checkpoint()):
        lhscheckpoint = lhs0
        interface.fulfilled_action(precice.action_write_iteration_checkpoint())
        
        
      lhs = solver.newton('lhs', res, constrain=cons, arguments=dict(lhs0=lhs0), lhs0=lhs0).solve(1e-8)
      
      x, p, ρ, u = bezier.eval(['x_i', 'p', 'ρ', 'u_i'] @ ns, lhs=lhs)
      
      writedataRight = p[199]
      interface.write_scalar_data(writedataRightID, dataIndexRight, writedataRight)

      writedataLeft = numpy.array([0.0, u[0], 0.0]) 
      interface.write_vector_data(writedataLeftID, dataIndexLeft, writedataLeft)      

      precice_dt = interface.advance(dt)
      
      # read checkpoint if required
      if interface.is_action_required(precice.action_read_iteration_checkpoint()):
        interface.fulfilled_action(precice.action_read_iteration_checkpoint())
        lhs0 = lhscheckpoint
      else:
        x, p, ρ, u = bezier.eval(['x_i', 'p', 'ρ', 'u_i'] @ ns, lhs=lhs)
        f.write("%e; %e; %e; %e; %e; %e; %e\n" % (t, p[0], u[0], p[199], u[199], p[99], u[99]))
        f.flush()
        t += dt
        n += 1
        lhs0 = lhs
      
  interface.finalize()
  f.close()


if __name__ == '__main__':
  cli.run(main)
