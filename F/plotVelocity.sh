#! /bin/bash
gnuplot -p << EOF
set grid
set xlabel 'Time [s]'
set ylabel 'Velocity'
set key right bottom
#set xrange [0.0:1.0]
plot "results/III/watchpoint.txt" using 1:7 with lines title "3D-1D-3D",\
     "../B/results/II/U" using 1:6 with lines title "monolithic 3D"
EOF

