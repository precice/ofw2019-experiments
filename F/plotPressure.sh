#! /bin/bash
gnuplot -p << EOF
set grid
set xlabel 'Time [s]'
set ylabel 'Pressure'
set xrange [7.0:8.0]
set yrange [280000:310000]

plot "results/III/p" using 1:2 with lines title "3D-1D-3D", \
     "../B/results/II/p" using 1:2 with lines title "OpenFOAM 3D Monolithic" 

EOF

