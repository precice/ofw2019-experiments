#! /bin/bash
gnuplot -p << EOF
set grid
set xlabel 'Time [s]' font ",12"
set ylabel 'Pressure [Pa]' font ",12" offset -3
set key center bottom 
#set key at graph 1.0, 0.2
set encoding utf8
set xtics font ",12" 
set ytics font ",12" 
set key font ",14" 
set format y "%.2t*10^%T";
set lmargin 15

#set xrange [7:8]
set xrange [0:20]
set yrange [-350000:350000]
#set yrange [285000:305000]

plot "../A/results/watchpointII.txt" using 1:2 with lines dt 1 lc rgb "black" lw 2 title "1D", \
     "../B/results/II/p" using 1:2 with lines dt 3 lc rgb "black" lw 2 title "3D", \
     "results/IV/watchpointRight.txt" using 1:4 with lines dt 1 lc rgb "dark-grey" lw 2 title "3D-1D", \
     "../D/results/III/p" using 1:2 with lines dt 3 lc rgb "dark-grey" lw 2 title "1D-3D", \
     "../F/results/III/p" using 1:2 with lines dt 1 lc rgb "light-grey" lw 2 title "3D-1D-3D", \
     "../G/results/watchpointRight.txt" using 1:4 with lines dt 3 lc rgb "light-grey" lw 2 title "1D-3D-1D"

EOF

