#! /bin/bash
gnuplot -p << EOF
set grid
set xlabel 'Time [s]' font ",12"
set ylabel 'Velocity [m/s]' font ",12" offset -0
set key center bottom
set encoding utf8
set xtics font ",12" 
set ytics font ",12" 
set key font ",14"
#set format y "%.1t*10^%T";
set lmargin 15

#set xrange [0:20]
set xrange [13.5:15.5]
set yrange [0.99:1.21]
#set yrange [0:1.3]

plot "results/IV/watchpointRight.txt" using 1:3 with lines title "Nutils",\
     "results/IV/U" using 1:3 with lines title "OpenFOAM", \
     "../B/results/II/U" using 1:6 with lines title "monolithic"
     
plot "../A/results/watchpointII.txt" using 1:3 with lines dt 1 lc rgb "black" lw 2 title "1D", \
     "../B/results/II/U" using 1:6 with lines dt 3 lc rgb "black" lw 2 title "3D", \
     "results/IV/watchpointRight.txt" using 1:3 with lines dt 1 lc rgb "dark-grey" lw 2 title "3D-1D", \
     "../D/results/III/U" using 1:6 with lines dt 3 lc rgb "dark-grey" lw 2 title "1D-3D", \
     "../F/results/III/watchpoint.txt" using 1:7 with lines dt 1 lc rgb "light-grey" lw 2 title "3D-1D-3D", \
     "../G/results/U" using 1:6 with lines dt 3 lc rgb "light-grey" lw 2 title "1D-3D-1D"     
     
     
EOF

