#! /bin/bash
gnuplot -p << EOF
set grid
set xlabel 'Time [s]'
set ylabel 'Pressure'
set xrange [7.0:8.0]
set yrange [280000:310000]

plot "results/I/p" using 1:2 with lines title "Aitken", \
     "results/II/p" using 1:2 with lines title "QN 3D Mesh", \
     "results/III/p" using 1:2 with lines title "QN 1D Mesh", \
     "../B/results/II/p" using 1:2 with lines title "OpenFOAM 3D" 

EOF

