#! /bin/bash
gnuplot -p << EOF
set grid
set xlabel 'Time [s]'
set ylabel 'Velocity'
set key right bottom
#set xrange [0.0:0.5]
plot "results/II/watchpointLeft.txt" using 1:5 with lines title "Nutils",\
     "results/II/U" using 1:6 with lines title "OpenFOAM", \
     "../B/results/II/U" using 1:6 with lines title "monolithic"
EOF

