# OFW2019-Experiments

Used codes:
* [preCICE commit](https://github.com/precice/precice/commit/f12e5bdb446a3a800334fa3fdc297cd9a013d66d)
* Nutils commit a360d091b3b92a29e31f8564bba9cf9c4b3fb08a
* OpenFOAM 5

## Water Hammer

* A: 1D Nutils monolithic with different time integration schemes
* B: 3D OpenFOAM monolithic
* C: 3D-3D parallel-explicit vs. parallel-implicit coupling 
* D: 1D-3D parallel-implicit: QN on 3D mesh vs QN on 1D mesh
* E: 3D-1D and 1D-1D, comparison of different setups
* F: 3D-1D-3D
* G: 1D-3D-1D

## Pipe-Pipe Incompressible Coupling

Only results, scenario setup is available throught the preCICE tutorials.
