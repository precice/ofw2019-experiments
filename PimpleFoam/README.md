withoutgradients: only pressure and velocity, dt 0.01
withgradients: additionally pressure gradient (not velocity gradient), dt 0.01
withgradients-small-dt: dt 0.002

explicit-coupling diverges!
