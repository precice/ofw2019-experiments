#! /bin/bash
gnuplot -p << EOF
set grid
set xlabel 'z [m]' font ",12"
set ylabel 'Velocity [m/s]' font ",12" offset -1
set key center bottom
set encoding utf8
set xtics font ",12" 
set ytics font ",12" 
set key font ",14" 
#set format y "%.1t*10^%T";
set lmargin 14

set datafile separator ","

set xrange [7:40]
#set xrange [13.5:15.5]
set yrange [0.197:0.201]

     
plot "withgradients1.csv" using 4:7 with lines dt 1 lc rgb "grey" lw 2 title "With Pressure Gradient, dt=0.01", \
     "withgradients2.csv" using 4:7 with lines dt 1 lc rgb "grey" lw 2 notitle, \
      "withgradients-small-dt1.csv" using 4:7 with lines dt 5 lc rgb "dark-grey" lw 2 title "With Pressure Gradient, dt=0.002", \
     "withgradients-small-dt2.csv" using 4:7 with lines dt 5 lc rgb "dark-grey" lw 2 notitle, \
     "withoutgradients1.csv" using 4:7 with lines dt 3 lc rgb "black" lw 2 title "No Gradients, dt=0.01", \
     "withoutgradients2.csv" using 4:7 with lines dt 3 lc rgb "black" lw 2 notitle


     
EOF

