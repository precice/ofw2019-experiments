#! /bin/bash
gnuplot -p << EOF
set grid
set xlabel 'Time [s]' font ",12"
set ylabel 'Pressure [Pa]' font ",12" offset -3
set key center bottom
set encoding utf8
set xtics font ",12" 
set ytics font ",12" 
set key font ",14"
set format y "%.1t*10^%T";
set lmargin 15

set xrange [6.8:8.2]
#set yrange [-350000:350000]
set yrange [220000:320000]


plot "results/watchpointII.txt" using 1:2 with lines dt 1 lc rgb "black" lw 2 title "1D dt=0.005",\
     "results/watchpointIV.txt" using 1:2 with lines dt 3 lc rgb "black" lw 2 title "1D dt=0.001",\
     "results/watchpointV.txt" using 1:2 with lines dt 3 lc rgb "grey" lw 2 title "fine mesh"
     
     
#     "../B/results/I/p" using 1:2 with lines dt 1 lc rgb "grey" lw 2 title "3D dt=0.005",\
#     "../B/results/III/p" using 1:2 with lines dt 3 lc rgb "grey" lw 2 title "3D dt=0.001",\
#     "../B/results/IV/p" using 1:2 with lines dt 2 lc rgb "gray40" lw 2 title "3D dt=0.005" fine mesh"
     
  
#plot "results/watchpointI.txt" using 1:2 with lines dt 1 lc rgb "grey" lw 2 title "1D Crank-Nicolson θ = 0.5",\
#     "results/watchpointII.txt" using 1:2 with lines dt 3 lc rgb "black" lw 2 title "1D Crank-Nicolson θ = 0.55",\
#     "results/watchpointIII.txt" using 1:2 with lines dt 2 lc rgb "dark-grey" lw 2 title "1D Backward Euler",\
#     "../B/results/II/p" using 1:2 with lines dt 4 lc rgb "gray40" lw 2 title "3D Monolithic"  
  
     
EOF

