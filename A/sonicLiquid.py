#! /usr/bin/python3

from nutils import *
import numpy, itertools, treelog

@function.replace
def subs0(f):
  if isinstance(f, function.Argument) and f._name == 'lhs':
    return function.Argument(name='lhs0', shape=f.shape, nderiv=f._nderiv)

def main(nelems=800, timestep=.005, refdensity=1e3, refpressure=101325., psi=1e-6, viscosity=1e-3, theta=1.0):

  domain, geom = mesh.rectilinear([numpy.linspace(0, 1000, nelems+1)])
  bezier = domain.sample('bezier', 2)

  ns = function.Namespace()
  ns._functions['t'] = lambda f: theta * f + (1-theta) * subs0(f)
  ns._functions_nargs['t'] = 1
  ns._functions['δt'] = lambda f: (f - subs0(f)) / timestep
  ns._functions_nargs['δt'] = 1
  ns.x = geom
  ns.ρref = refdensity
  ns.pref = refpressure
  ns.pin = 98100
  ns.μ = viscosity
  ns.ψ = psi
  ns.ρbasis, ns.ubasis = function.chain([domain.basis('std', degree=1), domain.basis('std', degree=2).vector(domain.ndims)])
  ns.ρ = 'ρref + ρbasis_n ?lhs_n' # density is shifted by ref density
  ns.u_i = 'ubasis_ni ?lhs_n'
  ns.p = 'pref + (ρ - ρref) / ψ' # pressure-density connection, see equ (8)
  ns.σ_ij = 'μ (u_i,j + u_j,i) - p δ_ij' # diffusive term and pressure gradient
  ns.h = 1 / nelems
  ns.k = 'ρ h / μ' # needs work, stabilization coeff

  res = domain.integral('ρbasis_n (δt(ρ) + t((ρ u_k)_,k)) d:x' @ ns, degree=4) # mass balance
  res += domain.integral('(ubasis_ni (δt(ρ u_i) + t((ρ u_i u_j)_,j)) + ubasis_ni,j t(σ_ij)) d:x' @ ns, degree=4) # momentum balance
  res += domain.boundary['left'].integral('pin ubasis_ni n_i d:x' @ ns, degree=4) # pressure set at inlet
  #res += domain.integral('k ubasis_ni,j (u_j / sqrt(u_k u_k)) (δρu_i / δt + (ρ u_i u_j - σ_ij)_,j) d:x' @ ns, degree=4) # SUPG stabilization

  lhs = numpy.zeros(res.shape)
  t = 0
  
  f = open("watchpoint.txt", "w")
  
  sqr = domain.boundary['right'].integral('(u_0 - 1)^2' @ ns, degree=4)
  cons0 = solver.optimize('lhs', sqr, droptol=1e-14)

  for istep in itertools.count():
    with log.context('timestep', istep):
   
      x, p, ρ, u = bezier.eval(['x_i', 'p', 'ρ', 'u_i'] @ ns, lhs=lhs)
      f.write("%e; %e; %e\n" % (t, p[1599], u[799]))
      f.flush()
      
      cons = min(.2 * t, 1) * cons0
        
      lhs = solver.newton('lhs', res, constrain=cons, arguments=dict(lhs0=lhs), lhs0=lhs).solve(1e-8)

      t += timestep
      
      if t>=20:
        break
  f.close()


if __name__ == '__main__':
  cli.run(main)
